Multithreading.

Task #11. Tunnel

There is two railroad tunnels, which can be crossed(traversed) by trains in both directions(from south to north and vice versa). There are queues of trains at both ends of tunnels. Task: ensure safe tunnels crossing for all trains. Train can be redirected to other tunnel if it has been waiting too long in current queue. 
