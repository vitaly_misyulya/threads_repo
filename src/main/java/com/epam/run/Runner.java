package com.epam.run;


import com.epam.creator.PassCreator;
import com.epam.entity.Pass;
import com.epam.entity.Train;
import com.epam.entity.Tunnel;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;

import java.util.LinkedList;
import java.util.concurrent.TimeUnit;


public class Runner {

    static {
        new DOMConfigurator().doConfigure("config/log4j.xml", LogManager.getLoggerRepository());
//        PropertyConfigurator.configure("config/log4j.properties");
    }

    public static void main(String[] args) {

        /*Pass pass = new Pass();
        Tunnel tunnel = new Tunnel(1, 1);
        pass.addTunnel(tunnel);
        LinkedList<Train> sQueue = new LinkedList<Train>();
        sQueue.add(new Train("1", 200, 2000));
        sQueue.add(new Train("3", 150, 2000));
        sQueue.add(new Train("5", 250, 750));
        sQueue.add(new Train("7", 200, 2000));
        tunnel.addTrainsToSouthQueue(sQueue);


        LinkedList<Train> nQueue = new LinkedList<Train>();
        nQueue.add(new Train("2", 225, 2000));
        nQueue.add(new Train("4", 175, 2000));
        nQueue.add(new Train("6", 400, 2000));
        nQueue.add(new Train("8", 225, 2000));
        nQueue.add(new Train("10", 225, 1000));
        nQueue.add(new Train("12", 125, 2000));
        nQueue.add(new Train("14", 75, 2000));
        tunnel.addTrainsToNorthQueue(nQueue);

        for (int i = 0; i < sQueue.size(); i++) {
            new Thread(sQueue.get(i)).start();
        }

        for (int i = 0; i < nQueue.size(); i++) {
            new Thread(nQueue.get(i)).start();
        }

        try {
            TimeUnit.MILLISECONDS.sleep(1200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Train train = new Train("11", 200, 2000);
        tunnel.addLastToSouthQueue(train);
        new Thread(train).start();*/

        Pass pass = PassCreator.createPass(4);
        pass.launchTrains();

    }
}
