package com.epam.util;


import java.util.Random;

public class Util {

    public static final Random RANDOM = new Random();

    public static int generateInt(int min, int max) {
        return RANDOM.nextInt((max - min) + 1) + min;
    }
}
