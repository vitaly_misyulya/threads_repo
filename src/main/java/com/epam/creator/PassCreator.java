package com.epam.creator;


import com.epam.entity.Pass;
import com.epam.entity.Train;
import com.epam.entity.Tunnel;
import com.epam.util.ResourceManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import com.epam.util.Util;

public class PassCreator {

    private static final ResourceManager RESOURCE_MANAGER = ResourceManager.INSTANCE;

    public static Pass createPass(int numOfTunnels) {
        Pass pass = new Pass();
        for (int i = 1; i <= numOfTunnels; i++) {
            pass.addTunnel(generateTunnel(i));
        }
        return pass;
    }

    private static Tunnel generateTunnel(int tunnelNumber) {
        int tunnelLength = Integer.parseInt(RESOURCE_MANAGER.getString("tunnel-length"));
        Tunnel tunnel = new Tunnel(tunnelNumber, tunnelLength);
        int minNumberOfTrains = Integer.parseInt(RESOURCE_MANAGER.getString("min-number-of-trains-in-tunnel"));
        int maxNumberOfTrains = Integer.parseInt(RESOURCE_MANAGER.getString("max-number-of-trains-in-tunnel"));
        int[] numOfTrains = new Random().ints(2, minNumberOfTrains, maxNumberOfTrains).toArray();
        tunnel.addTrainsToSouthQueue(generateTrains(tunnel, numOfTrains[0], 1));
        tunnel.addTrainsToNorthQueue(generateTrains(tunnel, numOfTrains[1], 2));
        return tunnel;
    }

    private static Collection<? extends Train> generateTrains(Tunnel tunnel, int numberOfTrains, int startNumber) {
//        List<Train> trainList = new ArrayList<Train>(numberOfTrains);
        List<Train> trainList = new ArrayList<Train>();
        int minSpeed = Integer.parseInt(RESOURCE_MANAGER.getString("train.min-speed"));
        int maxSpeed = Integer.parseInt(RESOURCE_MANAGER.getString("train.max-speed"));
        int minWaitInterval = Integer.parseInt(RESOURCE_MANAGER.getString("train.min-wait-interval"));
        int maxWaitInterval = Integer.parseInt(RESOURCE_MANAGER.getString("train.max-wait-interval"));

        for (int i = 0; i < numberOfTrains; i++) {
            int speed = Util.generateInt(minSpeed, maxSpeed);
            int waitInterval = Util.generateInt(minWaitInterval, maxWaitInterval);
            trainList.add(new Train(String.format("t%d_%02d", tunnel.getNumber(), startNumber), speed, waitInterval));
            startNumber += 2;
        }
        return trainList;
    }


}
