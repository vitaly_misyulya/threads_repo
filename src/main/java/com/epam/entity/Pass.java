package com.epam.entity;

import com.epam.util.Util;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;

public class Pass {

    private static final Logger LOG = Logger.getLogger(Train.class);

    private static final String TAG = "pass";

    private ArrayList<Tunnel> tunnels;

    public Pass() {
        tunnels = new ArrayList<Tunnel>();
    }

    public Pass(ArrayList<Tunnel> tunnels) {
        this.tunnels = tunnels;
    }

    public int numberOfTunnels() {
        return tunnels.size();
    }

    public boolean addTunnel(Tunnel tunnel) {
        tunnel.setPass(this);
        return tunnels.add(tunnel);
    }

    public boolean addTunnels(Collection<? extends Tunnel> c) {
        for (Tunnel tunnel : c) {
            tunnel.setPass(this);
        }
        return tunnels.addAll(c);
    }

    public void launchTrains() {
        for (Tunnel tunnel : tunnels) {
            tunnel.launchTrains();
        }
    }

    public void redirectTrainFromSouthQueue(Train train) {
        int ind = tunnels.indexOf(train.getTunnel());
        int nextTunnel = Util.RANDOM.nextInt(tunnels.size() - 1);
        if (nextTunnel >= ind) {
            nextTunnel++;
        }
//        LOG.info(String.format("%-7s : Train #%s was redirected from south queue of tunnel#%d to tunnel#%d", TAG, train.getName(), ind + 1, nextTunnel + 1));
        LOG.info(String.format("%-7s : #%s #%d =====> #%d", TAG, train.getName(), ind + 1, nextTunnel + 1));
        tunnels.get(nextTunnel).addLastToSouthQueue(train);
        new Thread(train).start();
    }

    public void redirectTrainFromNorthQueue(Train train) {
        if (tunnels.size() > 1) {
            int ind = tunnels.indexOf(train.getTunnel());
            int nextTunnel = Util.RANDOM.nextInt(tunnels.size() - 1);
            if (nextTunnel >= ind) {
                nextTunnel++;
            }
//        LOG.info(String.format("%-7s : Train #%s was redirected from north queue of tunnel#%d to tunnel#%d", TAG, train.getName(), ind + 1, nextTunnel + 1));
            LOG.info(String.format("%-7s : #%s #%d =====> #%d", TAG, train.getName(), ind + 1, nextTunnel + 1));
            tunnels.get(nextTunnel).addLastToNorthQueue(train);
            new Thread(train).start();
        } else {
            LOG.info(String.format("%-7s : No tunnels to redirect train #%s. Just removing it.", TAG, train.getName()));
        }
    }
}
