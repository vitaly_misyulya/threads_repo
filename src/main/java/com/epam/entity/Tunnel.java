package com.epam.entity;


import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Tunnel {

    private static final Logger LOG = Logger.getLogger(Tunnel.class);

    private static final String TAG = "tunnel";

    private int number;
    private int length;

    private Pass pass;

    private LinkedList<Train> southEntranceQueue;
    private LinkedList<Train> northEntranceQueue;

    private Lock tunnelLock = new ReentrantLock();
    private Lock sQueueLock = new ReentrantLock();
    private Lock nQueueLock = new ReentrantLock();

//    public Tunnel(int number, int length, Pass pass) {
    public Tunnel(int number, int length) {
        this.number = number;
        this.length = length;
//        this.pass = pass;
        southEntranceQueue = new LinkedList<Train>();
        northEntranceQueue = new LinkedList<Train>();
    }

    public int getNumber() {
        return number;
    }

    public void setPass(Pass pass) {
        this.pass = pass;
    }

    public void launchTrains() {
        for (Train train : southEntranceQueue) {
            new Thread(train).start();
        }
        for (Train train : northEntranceQueue) {
            new Thread(train).start();
        }
    }

    public boolean addTrainsToSouthQueue(Collection<? extends Train> c) {
        sQueueLock.lock();
        if (southEntranceQueue.isEmpty()) {
            c.iterator().next().setIsFirstInQueue(true);
        }
        for (Train t : c) {
            t.setTunnel(this);
        }
        boolean result = southEntranceQueue.addAll(c);
        sQueueLock.unlock();
        return result;
    }

    public boolean addTrainsToNorthQueue(Collection<? extends Train> c) {
        boolean result = false;
        nQueueLock.lock();
        if (northEntranceQueue.isEmpty()) {
            c.iterator().next().setIsFirstInQueue(true);
        }
        for (Train t : c) {
            t.setTunnel(this);
        }
        result = northEntranceQueue.addAll(c);
        nQueueLock.unlock();
        return result;
    }

    public void addLastToSouthQueue(Train train) {
        sQueueLock.lock();
        if (southEntranceQueue.isEmpty()) {
            train.setIsFirstInQueue(true);
        }
        train.setTunnel(this);
        southEntranceQueue.addLast(train);
        sQueueLock.unlock();
    }

    public void addLastToNorthQueue(Train train) {
        nQueueLock.lock();
        if (northEntranceQueue.isEmpty()) {
            train.setIsFirstInQueue(true);
        }
        train.setTunnel(this);
        northEntranceQueue.addLast(train);
        nQueueLock.unlock();
    }

    public void traverse(Train train) {
//        LOG.info(String.format("tunnel: Train #%s trying to acquire tunnel lock", train.getName()));
        tunnelLock.lock();
//        LOG.info(String.format("tunnel: Train #%s acquired tunnel lock", train.getName()));
        try {
//            LOG.info(String.format("%-7s : Train #%s entering tunnel #%d >>> %d >>>", TAG, train.getName(), number, number));
            LOG.info(String.format("%-7s : #%s  >>> %d >>>", TAG, train.getName(), number));
            TimeUnit.MILLISECONDS.sleep(train.getSpeed() / length);
//            LOG.info(String.format("%-7s : Train #%s leaving tunnel #%d <<< %d <<<", TAG, train.getName(), number, number));
            LOG.info(String.format("%-7s : #%s  <<< %d <<<", TAG, train.getName(), number));

//            if (!southEntranceQueue.isEmpty() && train == southEntranceQueue.getFirst()) {
            if (train == southEntranceQueue.peek()) {
                shiftSouthQueue();
            } else if (train == northEntranceQueue.peek()) {
                shiftNorthQueue();
            } else {
                LOG.error(String.format("%-7s : Tunnel #%d Error: Train #%s doesn't belong any queue", TAG, number, train.getName()));
            }
        } catch (InterruptedException e) {
            LOG.error("tunnel: Tunnel Error: " + e);
        } finally {
            tunnelLock.unlock();
        }
    }

    private void shiftSouthQueue() {
        sQueueLock.lock();
//      LOG.info("traverse: removing first from south queue");
        southEntranceQueue.remove();
        if (!southEntranceQueue.isEmpty()) {
//            LOG.info(String.format("tunnel: Signaling Train %s that he is first", southEntranceQueue.getFirst().getName()));
            southEntranceQueue.getFirst().signalFirstInQueue();
        } else {
            LOG.info(String.format("%-7s : ***** South queue of tunel #%d is empty", TAG, number));
        }
        sQueueLock.unlock();
    }

    private void shiftNorthQueue() {
        nQueueLock.lock();
//      LOG.info("traverse: removing first from north queue");
        northEntranceQueue.remove();
        if (!northEntranceQueue.isEmpty()) {
//          LOG.info(String.format("traverse: Signaling Train %s that he is first", northEntranceQueue.getFirst().getName()));
            northEntranceQueue.getFirst().signalFirstInQueue();
        } else {
            LOG.info(String.format("%-7s : ***** North queue of tunel #%d is empty", TAG, number));
        }
        nQueueLock.unlock();
    }

    public void removeTrain(Train train) {
        if (southEntranceQueue.contains(train)) {
            sQueueLock.lock();
            southEntranceQueue.remove(train);
            sQueueLock.unlock();
        } else if (northEntranceQueue.contains(train)) {
            nQueueLock.lock();
            northEntranceQueue.remove(train);
            nQueueLock.unlock();
        }
    }

    public void redirectTrain(Train train) {
        if (southEntranceQueue.contains(train)) {
            sQueueLock.lock();
            pass.redirectTrainFromSouthQueue(train);
            if (!southEntranceQueue.remove(train)) {
                LOG.info(String.format("%-7s : Can't delete train #%s from south queue of tunnel #%d", TAG, train.getName(), number));
            }
            sQueueLock.unlock();
        } else if (northEntranceQueue.contains(train)) {
            nQueueLock.lock();
            pass.redirectTrainFromNorthQueue(train);
            if (!northEntranceQueue.remove(train)) {
                LOG.info(String.format("%-7s : Can't delete train #%s from north queue of tunnel #%d", TAG, train.getName(), number));
            }
            nQueueLock.unlock();
        }
    }
}
