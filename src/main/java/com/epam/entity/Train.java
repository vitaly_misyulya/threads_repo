package com.epam.entity;


import com.epam.util.ResourceManager;
import org.apache.log4j.Logger;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Train implements Runnable {

    private static final Logger LOG = Logger.getLogger(Train.class);
    private static final ResourceManager RESOURCE_MANAGER = ResourceManager.INSTANCE;

    private static final String TAG = "train";

    private String name;
    private int speed;
    private int waitTimeout;

    private volatile boolean isFirstInQueue;

    private Tunnel tunnel;

    private Lock trainLock;
    private Condition firstInQueue;

//    public Train(String name, int speed, int waitTimeout, Tunnel tunnel) {
    public Train(String name, int speed, int waitTimeout) {
        this.name = name;
        this.speed = speed;
        this.waitTimeout = waitTimeout;

//        this.tunnel = tunnel;

        trainLock = new ReentrantLock();
        firstInQueue = trainLock.newCondition();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getWaitTimeout() {
        return waitTimeout;
    }

    public void setWaitTimeout(int waitTimeout) {
        this.waitTimeout = waitTimeout;
    }

    public void setIsFirstInQueue(boolean isFirstInQueue) {
        this.isFirstInQueue = isFirstInQueue;
    }

    public Tunnel getTunnel() { return tunnel; }

    public void setTunnel(Tunnel tunnel) { this.tunnel = tunnel; }

    public void run() {
        LOG.info(String.format("%-7s : Train #%s preparing", TAG ,getName()));
        try {
            int maxTrainPreparingTime = Integer.parseInt(RESOURCE_MANAGER.getString("train.max-preparing-time"));
            TimeUnit.MILLISECONDS.sleep(new Random().nextInt(maxTrainPreparingTime));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        LOG.info(String.format("%-7s : Train #%s start running", TAG, getName()));

        trainLock.lock();
//        LOG.info(String.format("train: Train #%s acquired train trainLock", getName()));
        try {
//            LOG.info(String.format("train: Train #%s checks whether it good to go ", getName()));
            if (isFirstInQueue || firstInQueue.await(waitTimeout, TimeUnit.MILLISECONDS)) {
//                LOG.info(String.format("train: Train #%s is first now so going to traverse ", getName()));
                tunnel.traverse(this);
            } else {
//                LOG.info(String.format("%-7s : Train #%s was waiting too long...", TAG, name));
//                tunnel.removeTrain(this);
                tunnel.redirectTrain(this);
            }
        } catch (InterruptedException e) {
            LOG.error("train: Train Error: " + e);
        } finally {
            trainLock.unlock();
        }

    }

    public void signalFirstInQueue() {
        trainLock.lock();
        firstInQueue.signal();
        isFirstInQueue = true;
        trainLock.unlock();
    }
}
